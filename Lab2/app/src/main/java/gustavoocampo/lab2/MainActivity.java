package gustavoocampo.lab2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    //Declare our View variables
    private EditText vInputEditText;
    private TextView vCopyTextView;
    private Button vCopyButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Assign the view from the layout file to the corresponding variables
        vInputEditText = (EditText) findViewById(R.id.user_input);
        vCopyTextView = (TextView) findViewById(R.id.CopyTextBox);
        vCopyButton = (Button) findViewById(R.id.CopyInputButton);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // The Button was pressed so copy the user input and paste it to the copyTextBox
                String input = vInputEditText.getText().toString();
                vCopyTextView.setText(input);
            }
        };
        vCopyButton.setOnClickListener(listener);
    }
}
