package cse455inc.lab3;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class MainActivity extends AppCompatActivity {
    EditText vInputEditText;
    TextView vCopyTextView;
    String jsonString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void buttonClick(View v){
        vInputEditText = (EditText) findViewById(R.id.user_input);
        jsonString = vInputEditText.getText().toString();
        new sendPostRequest().execute("http://date.jsontest.com");
    }
    public class sendPostRequest extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            String data = "";
            String result = "";
            String line;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection)url.openConnection();
                int code = connection.getResponseCode();
                if (code == 200) {
                    InputStream input = new BufferedInputStream(connection.getInputStream());
                    if(input != null) {
                        BufferedReader bReader = new BufferedReader(new InputStreamReader(input));
                        data = "";
                        while ((line = bReader.readLine()) != null)
                            data += line;
                    }
                    input.close();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }
            //Generate a new JSONObject with the data read in
            //from the BufferedReader.
            try {
                JSONObject jsonObject = new JSONObject(data);
                result = jsonObject.get(jsonString).toString();
            } catch(JSONException e) {
                e.printStackTrace();
            }
            return result; //You are returning the result to the onPostExecute method following below.
        }
        @Override
        protected void onPostExecute(String result) {
            vCopyTextView = (TextView) findViewById(R.id.CopyTextBox);
            vCopyTextView.setText(result);
        }
    }
}
